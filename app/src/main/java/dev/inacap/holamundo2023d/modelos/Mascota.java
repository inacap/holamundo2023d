package dev.inacap.holamundo2023d.modelos;

public class Mascota {
    public String nombre;
    public String tipo;

    public Mascota() {
    }

    public Mascota(String nombre, String tipo) {
        this.nombre = nombre;
        this.tipo = tipo;
    }
}
