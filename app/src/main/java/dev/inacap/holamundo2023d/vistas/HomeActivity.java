package dev.inacap.holamundo2023d.vistas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import dev.inacap.holamundo2023d.MainActivity;
import dev.inacap.holamundo2023d.R;
import dev.inacap.holamundo2023d.modelos.Mascota;

public class HomeActivity extends AppCompatActivity {

    public FirebaseFirestore mDataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Conexion con Firebase
        FirebaseApp.initializeApp(this);
        // Conexion con auth
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        // Conectar con BD (Firestore)
        mDataBase = FirebaseFirestore.getInstance();

        this.cargarDatos();

        FirebaseUser usuario = mAuth.getCurrentUser();

        TextView tvEmail = findViewById(R.id.tv_email);
        tvEmail.setText(usuario.getEmail());

        TextView tvBienvenida = findViewById(R.id.tv_bienvenida);

        Button btnSalir = findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Finalizar esta activity
                mAuth.signOut();
                Toast.makeText(HomeActivity.this, "Hasta pronto", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        Button btnCrear = findViewById(R.id.bt_crear);
        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText etNombre = findViewById(R.id.et_nombre);
                String nombre = etNombre.getText().toString().trim();

                // Creo la mascota
                Mascota m = new Mascota(nombre, "perro");

                // Crear el registro
                mDataBase.collection("mascotas").add(m).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(HomeActivity.this, "Mascota guardada", Toast.LENGTH_SHORT).show();
                            cargarDatos();
                        } else {
                            Toast.makeText(HomeActivity.this, "Error al guardar", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    public void cargarDatos(){
        // Cargar datos de mascotas
        mDataBase.collection("mascotas").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                List<DocumentSnapshot> listaMascotas = queryDocumentSnapshots.getDocuments();
                List<String> listaSimpleMascotas = new ArrayList<String>();

                for(int contador = 0; contador < listaMascotas.size(); contador ++){
                    Mascota mas = listaMascotas.get(contador).toObject(Mascota.class);
                    Log.i("MASCOTA", "Nombre de la mascota: " + mas.nombre);

                    // llenar la lista simple
                    listaSimpleMascotas.add(mas.nombre + " - " + mas.tipo);
                }

                // Crear el adaptador de la lista
                ArrayAdapter<String> adaptador = new ArrayAdapter<String>(
                        HomeActivity.this,
                        android.R.layout.simple_list_item_1,
                        listaSimpleMascotas
                );

                ListView listaVisual = findViewById(R.id.lv_mascotas);
                listaVisual.setAdapter(adaptador);
            }
        });
    }

}