package dev.inacap.holamundo2023d.vistas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import dev.inacap.holamundo2023d.R;

public class RegistrarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        // Conectar con los servicios de Firebase
        FirebaseApp.initializeApp(this);
        // Conectar con el servicio de Auth
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        Button btGuardar = findViewById(R.id.bt_guardar);
        btGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText campoEmail = findViewById(R.id.et_email);
                EditText campoPass1 = findViewById(R.id.et_pass1);
                EditText campoPass2 = findViewById(R.id.et_pass2);

                // Obtener los datos
                String email = campoEmail.getText().toString().trim();
                String pass1 = campoPass1.getText().toString().trim();
                String pass2 = campoPass2.getText().toString().trim();

                // Validacion de datos
                if(email == null || email.equals("")){
                    // Detecto que no ingresaron el email
                    campoEmail.setError("Campo obligatorio");
                    return;
                }

                if(pass1 == null || pass1.equals("")){
                    campoPass1.setError("Campo obligatorio");
                    return;
                }

                if(pass1.length() < 6){
                    campoPass1.setError("Debe tener minimo 6 caracteres");
                    return;
                }

                if(!pass1.equals(pass2)){
                    campoPass2.setError("Contraseñas no coinciden");
                    return;
                }

                // Crear cuenta
                mAuth
                        .createUserWithEmailAndPassword(email, pass1)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(RegistrarActivity.this, "Cuenta creada", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(RegistrarActivity.this, "No se pudo crear la cuenta.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }
}