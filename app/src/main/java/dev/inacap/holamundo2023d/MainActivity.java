package dev.inacap.holamundo2023d;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import dev.inacap.holamundo2023d.vistas.HomeActivity;
import dev.inacap.holamundo2023d.vistas.RegistrarActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Conectar Firebase
        FirebaseApp.initializeApp(this);

        // Conectar servicio de Auth
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        // Pedir los datos de quien inicio sesion
        FirebaseUser usuario = mAuth.getCurrentUser();
        if(usuario != null){
            // Alguien ya inicio sesion
            Intent i = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
            return;
        }

        TextView tvCrear = findViewById(R.id.tv_crear);
        tvCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Abrir ventana de registro
                Intent intencion = new Intent(MainActivity.this, RegistrarActivity.class);
                startActivity(intencion);
            }
        });

        Button btIniciar = findViewById(R.id.bt_guardar);
        btIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText campoEmail = findViewById(R.id.et_email);
                EditText campoPass = findViewById(R.id.et_pass1);

                String email = campoEmail.getText().toString().trim();
                String pass = campoPass.getText().toString().trim();

                if(email.equals("")){
                    campoEmail.setError("Campo obligatorio");
                    return;
                }

                if(pass.equals("")){
                    campoPass.setError("Campo obligatorio");
                    return;
                }

                mAuth
                        .signInWithEmailAndPassword(email, pass)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    // Inicio sesion, paso a HomeActivity
                                    Intent i = new Intent(MainActivity.this, HomeActivity.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    campoEmail.setError("Email o contraseña incorrectos");
                                }
                            }
                        });
            }
        });

    }
}